type t = {
    mutable pos: Vec2.t;
    mutable rad: float;
    mutable vel: Vec2.t;
};;

val from_pos_rad : Vec2.t -> float -> t;;
val update : t -> dt:float -> t;;
val render : t -> Sdlrender.t -> unit;;