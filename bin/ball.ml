type t = {
  mutable pos: Vec2.t;
  mutable rad: float;
  mutable vel: Vec2.t;
};;


let from_pos_rad (pos: Vec2.t) (rad: float) : t =
  {
    pos;
    rad;
    vel = Vec2.from_xy 10. 0.;
  }
;;


let update (ball: t) ~(dt: float) : t =
  { ball with
    pos = Vec2.(ball.pos + ball.vel*dt);
  }
;;


let render (ball: t) (rndr: Sdlrender.t) : unit =
  let x = int_of_float ball.pos.x
  and y = int_of_float ball.pos.y
  and r = int_of_float ball.rad in
  let rect = Sdlrect.make ~pos:(x - r, y - r) ~dims:(r*2, r*2) in
  Sdlrender.draw_rect rndr rect
;;