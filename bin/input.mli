type t = {
    quit: bool;

    paddle1_up: bool;
    paddle1_down: bool;
    paddle2_up: bool;
    paddle2_down: bool;
};;

val default : t;;
val process : t -> t;;