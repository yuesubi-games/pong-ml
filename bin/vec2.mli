type t = {
    mutable x: float;
    mutable y: float;
};;

val from_xy : float -> float -> t;;
val ( + ) : t -> t -> t;;
val ( * ) : t -> float -> t;;