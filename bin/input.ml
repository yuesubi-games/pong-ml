type t = {
    quit: bool;

    paddle1_up: bool;
    paddle1_down: bool;
    paddle2_up: bool;
    paddle2_down: bool;
};;


let default : t = {
  quit = false;

  paddle1_up = false;
  paddle1_down = false;
  paddle2_up = false;
  paddle2_down = false;
};;


let process (start_inputs: t) : t =
  let inputs_with_keycode_to (inputs: t) (kc: Sdlkeycode.t) (value: bool) : t =
    match kc with
    | Z    -> { inputs with paddle1_up = value }
    | S    -> { inputs with paddle1_down = value }
    | Up   -> { inputs with paddle2_up = value }
    | Down -> { inputs with paddle2_down = value }
    | _ -> inputs
  in

  let handle_event (inputs: t) : Sdlevent.t -> t =
    function
    | KeyUp e -> inputs_with_keycode_to inputs e.keycode false
    | KeyDown e -> inputs_with_keycode_to inputs e.keycode true
    | Quit _ -> { inputs with quit = true }
    | _ -> inputs
  in

  let rec loop (inputs: t) : t =
    match Sdlevent.poll_event () with
    | Some ev -> handle_event inputs ev |> loop 
    | None -> inputs
  in

  loop start_inputs
;;