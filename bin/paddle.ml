type t = {
  mutable pos: Vec2.t;
  mutable dim: Vec2.t;
};;


let from_pos_dim (pos: Vec2.t) (dim: Vec2.t) : t =
  { pos; dim; }
;;


let update (paddle: t) ~(dt: float) : unit =
  ignore (paddle, dt);
;;


let render (paddle: t) (rndr: Sdlrender.t) : unit =
  let x = int_of_float paddle.pos.x
  and y = int_of_float paddle.pos.y
  and w = int_of_float paddle.dim.x
  and h = int_of_float paddle.dim.y in
  let rect = Sdlrect.make ~pos:(x - w/2, y - h/2) ~dims:(w, h) in
  Sdlrender.draw_rect rndr rect
;;