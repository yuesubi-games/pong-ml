type t = {
  mutable x: float;
  mutable y: float;
};;


let from_xy (x: float) (y: float) : t =
  { x; y; }
;;


let ( + ) (a: t) (b: t) : t =
  {
    x = a.x +. b.x;
    y = a.y +. b.y;
  }
;;


let ( * ) (vec: t) (scalar: float) : t =
  {
    x = vec.x *. scalar;
    y = vec.y *. scalar;
  }
;;