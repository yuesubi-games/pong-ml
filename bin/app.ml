type t = {
  sdl_rndr: Sdlrender.t;

  inputs: Input.t;

  last_tick: int;
  dt: float;

  ball: Ball.t;
  paddle1: Paddle.t;
  paddle2: Paddle.t;
};;


let init () : t =
  Sdl.init [`VIDEO];

  let sdl_win = Sdlwindow.create2
    ~title:"Hello world"
    ~x:`undefined ~y:`undefined ~width:640 ~height:480
    ~flags:[]
  in

  let sdl_rndr = Sdlrender.create_renderer
    ~win:sdl_win ~index:0 ~flags:[Sdl.Render.Accelerated]
  in

  {
    sdl_rndr;

    inputs = Input.default;

    last_tick = Sdltimer.get_ticks ();
    dt = 0.;

    ball = Ball.from_pos_rad
      (Vec2.from_xy 320. 240.)
      16.;

    paddle1 = Paddle.from_pos_dim
      (Vec2.from_xy 40. 240.)
      (Vec2.from_xy 20. 100.);
    paddle2 = Paddle.from_pos_dim
      (Vec2.from_xy 600. 240.)
      (Vec2.from_xy 20. 100.);
  }
;;


(*let draw_circle (rndr: Sdlrender.t) (center: int * int) (radius: int) : unit =
  let (x0, y0) = radius in

  let rec loop (x: int) (y: int) (dx: int) (dy: int) (err: int) =
    let draw_pixel = Sdlrender.draw_point rndr in

    draw_pixel (x0 + x, y0 + y);
    draw_pixel (x0 + y, y0 + x);
    draw_pixel (x0 - y, y0 + x);
    draw_pixel (x0 - x, y0 + y);
    draw_pixel (x0 - x, y0 - y);
    draw_pixel (x0 - y, y0 - x);
    draw_pixel (x0 + y, y0 - x);
    draw_pixel (x0 + x, y0 - y);
  in

  loop (radius - 1) 0 1 1 (1 - 2*radius)
;;*)


let handle_time (app: t) : t =
  let new_tick = Sdltimer.get_ticks () in
  { app with
    dt = (float_of_int (new_tick - app.last_tick)) /. 1000.;
    last_tick = new_tick
  }
;;


let handle_events (app: t) : t =
  { app with
    inputs = Input.process app.inputs
  }
;;


let update (app: t) : t =
  if app.dt == 0. then
    Printf.printf "fps: NaN"
  else
    Printf.printf "fps: %f" (1. /. app.dt);
  print_newline ();

  let ball = Ball.update app.ball ~dt:app.dt in
  Paddle.update app.paddle1 ~dt:app.dt;
  Paddle.update app.paddle2 ~dt:app.dt;

  if app.inputs.paddle1_down then
    app.paddle1.pos.y <- app.paddle1.pos.y +. 100. *. app.dt;
  if app.inputs.paddle1_up then
    app.paddle1.pos.y <- app.paddle1.pos.y -. 100. *. app.dt;

  if app.inputs.paddle2_down then
    app.paddle2.pos.y <- app.paddle2.pos.y +. 100. *. app.dt;
  if app.inputs.paddle2_up then
    app.paddle2.pos.y <- app.paddle2.pos.y -. 100. *. app.dt;

  Sdltimer.delay ~ms:10;
  { app with
    ball
  }
;;


let render (app: t) : t =
  Sdlrender.set_draw_color app.sdl_rndr ~rgb:(0, 0, 0) ~a:255;
  Sdlrender.clear app.sdl_rndr;

  Sdlrender.set_draw_color app.sdl_rndr ~rgb:(255, 0, 255) ~a:255;
  Ball.render app.ball app.sdl_rndr;
  Paddle.render app.paddle1 app.sdl_rndr;
  Paddle.render app.paddle2 app.sdl_rndr;

  Sdlrender.render_present app.sdl_rndr;

  app
;;


let rec main_loop (app: t) : t =
  if app.inputs.quit then
    app
  else
    app |>
    handle_time |>
    handle_events |>
    update |>
    render |>
    main_loop
;;


let destroy (_app: t) : unit =
  Sdl.quit ()
;;


let run () : unit =
  init () |>
  main_loop |>
  destroy
;;