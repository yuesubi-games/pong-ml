type t = {
    mutable pos: Vec2.t;
    mutable dim: Vec2.t;
};;

val from_pos_dim : Vec2.t -> Vec2.t -> t;;
val update : t -> dt: float -> unit;;
val render : t -> Sdlrender.t -> unit;;