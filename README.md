# Pong Ml
A Pong clone in OCaml and SDL2


## Run
Install `dune`, `ocamlsdl2` and then :
```sh
dune exec pong_ml
```


## Architecture

### Modules
* Ball
* Paddle
* App